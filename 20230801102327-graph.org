:PROPERTIES:
:ID:       81c87334-cbfd-4b3c-8c15-88508b4a4448
:END:
#+title: graph

graph is a [[id:37f04ecb-7ec0-49d0-a18d-14fadcdba4a6][structure]] that contains nodes (also called vertices
or edges) and their connections, edges.

there are several ways to store graph, but the most usefull is
list that contains edges of the node.

#+begin_src c

  typedef ListU40 **Graph;
  /*
    0 - 1, 2, 3
    1 - 0, 4
    2 - 0, 4
    3 - 0
    4 - 1, 2
   */

#+end_src
